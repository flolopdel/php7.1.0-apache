#!/bin/bash
set -e

# Set the timezone
# echo ${TZ} >/etc/timezone \
# 	&& dpkg-reconfigure -f noninteractive tzdata

# SSMTP / sendmail PHP config
if [[ ! $PHP_MAIL_USERNAME ]] || [[ $PHP_MAIL_USERNAME = 'null' ]]; then
	{ \
		echo "mailhub=${PHP_MAIL_IP}:${PHP_MAIL_PORT}"; \
		echo "FromLineOverride=YES"; \
	} > /etc/ssmtp/ssmtp.conf
else
	{ \
		echo "mailhub=${PHP_MAIL_IP}:${PHP_MAIL_PORT}"; \
		echo "AuthUser=${PHP_MAIL_USERNAME}"; \
		echo "AuthPass=${PHP_MAIL_PASSWORD}"; \
		echo "UseTLS=${PHP_MAIL_USE_TLS}"; \
		echo "UseSTARTTLS=${PHP_MAIL_USE_TLS}"; \
		echo "FromLineOverride=YES"; \
	} > /etc/ssmtp/ssmtp.conf
fi

{ \
	echo "sendmail_path = /usr/sbin/ssmtp -t"; \
} > /usr/local/etc/php/conf.d/sendmail.ini


# XDebug config
if [ "$PHP_XDEBUG_ENABLE" = 'On' ]; then

	{ \
#		echo "zend_extension = xdebug.so"; \
		echo "xdebug.profiler_enable = ${PHP_XDEBUG_ENABLE}"; \
		echo "xdebug.remote_enable = ${PHP_XDEBUG_ENABLE}"; \
		echo "xdebug.remote_autostart = 0"; \
		echo "xdebug.remote_port = 9001"; \
		echo "xdebug.remote_handler = 'dbgp'"; \
		echo "xdebug.remote_mode = req"; \
		echo "xdebug.remote_log = '/tmp/xdebug.log'"; \
		echo "xdebug.remote_host = ${PHP_XDEBUG_REMOTE_HOST}"; \
		echo "xdebug.remote_connect_back = 0"; \
		echo "xdebug.idekey = sublime.xdebug"; \
		echo "xdebug.halt_level = ''"; \
	} > /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini;

fi

# OpCache config
# see https://secure.php.net/manual/en/opcache.installation.php
{ \
	echo "opcache.enable=${PHP_OPCACHE_ENABLE}"; \
	echo "opcache.load_comments=1 "; \
	echo 'opcache.memory_consumption=192'; \
	echo 'opcache.interned_strings_buffer=16'; \
	echo 'opcache.max_accelerated_files=7963'; \
	echo 'opcache.validate_timestamps=0'; \
	echo 'opcache.revalidate_freq=0'; \
	echo 'opcache.fast_shutdown=1'; \
#    echo 'opcache.enable_cli=1'; \
} > /usr/local/etc/php/conf.d/opcache-recommended.ini

# ZLib config
{ \
	echo "zlib.output_compression=${PHP_ZLIB_OUTPUT_COMPRESSION_ENABLE}"; \
	echo "zlib.output_compression_level=${PHP_ZLIB_OUTPUT_COMPRESSION_LEVEL}"; \
#	echo "zlib.output_handler = ob_gzhandler"; \
	echo "zlib.output_handler = "; \
} > /usr/local/etc/php/conf.d/docker-php-custom-zlib.ini

# Custom misc
{ \
	echo "date.timezone=${TZ}"; \
	echo "expose_php=Off"; \
	echo "upload_max_filesize=30M"; \
	echo "memory_limit=256M"; \
	echo "post_max_size=30M"; \
	echo "max_input_vars=2000"; \
	echo "log_errors=On"; \
	echo "error_log=/dev/stderr"; \
} > /usr/local/etc/php/conf.d/docker-php-custom-misc.ini
